package com.boomi.connector.ethereum;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;

public class EthereumConnector extends BaseConnector {
	@Override
	public Browser createBrowser(BrowseContext context) {
		return new EthereumBrowser(createConnection(context));
	}

	@Override
	protected Operation createExecuteOperation(OperationContext context) {
		String customOperationType = context.getCustomOperationType();
		
		if (customOperationType.equals(EthereumOperationType.INVOKE_CONSTANT.name())) {
			return new EthereumInvokeOperation(createConnection(context), EthereumOperationType.INVOKE_CONSTANT);
		} else if (customOperationType.equals(EthereumOperationType.INVOKE_NONCONSTANT.name())) {
			return new EthereumInvokeOperation(createConnection(context), EthereumOperationType.INVOKE_NONCONSTANT);
		} else {
			throw new UnsupportedOperationException();
		}
	}
	

    private EthereumConnection createConnection(BrowseContext context) {
        return new EthereumConnection(context);
    }
}
