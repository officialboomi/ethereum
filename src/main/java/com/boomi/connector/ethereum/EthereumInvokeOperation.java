package com.boomi.connector.ethereum;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.web3j.abi.TypeMappingException;
import org.web3j.abi.TypeReference;
import org.web3j.abi.Utils;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.DynamicBytes;
import org.web3j.abi.datatypes.NumericType;
import org.web3j.abi.datatypes.StaticArray;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.AbiTypes;
import org.web3j.utils.Numeric;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.ethereum.decode.FunctionReturnType;
import com.boomi.connector.ethereum.decode.TypeDecoder;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.StringUtil;

public class EthereumInvokeOperation extends BaseUpdateOperation {
    private static final Logger LOG = Logger.getLogger("com.boomi.connector.ethereum");
	
    private static final String CONTRACT_ADDRESS = "contractAddress";
    private static final String FUNCTION = "function";
    private static final String RETURN_TYPES = "returnTypes";
    private static final String TYPE = "type";
    private static final String VALUE = "value";
    private static final String ARGS = "args";
    private static final String STATIC_ARRAY_CLASS_PREFIX = "org.web3j.abi.datatypes.generated.StaticArray";
    
    final static Pattern pattern = Pattern.compile("(\\w+)(?:\\[(.*?)\\])(?:\\[(.*?)\\])?");    

    private EthereumOperationType _type;
    
	protected EthereumInvokeOperation(EthereumConnection conn, EthereumOperationType type) {
		super(conn);
		_type = type;
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		PropertyMap opProperties = getContext().getOperationProperties();
		
		String functionName = opProperties.getProperty(FUNCTION);
        if (StringUtil.isEmpty(functionName)) {
            throw new ConnectorException("A Function Name value must be provided");
        }
		
		for (ObjectData od : request) {
			try {
				String data = toString(od.getData());
				JSONObject jsonIn = new JSONObject(data);
				if (!jsonIn.has(CONTRACT_ADDRESS)) {
					response.addErrorResult(od, OperationStatus.FAILURE, String.valueOf(404), "Incoming JSON Object does not contain a contractAddress property", null);
					continue;
				}
				if (!jsonIn.has(ARGS)) {
					response.addErrorResult(od, OperationStatus.FAILURE, String.valueOf(404), "Incoming JSON Object does not contain an args array property", null);
					continue;
				}
				
				String contractAddress = jsonIn.getString(CONTRACT_ADDRESS);
				
				JSONArray args = jsonIn.getJSONArray(ARGS);
				
				try {
					List<Type> inputParameters = buildInputParameters(args);
					switch (_type) {
						case INVOKE_NONCONSTANT: {
							LOG.log(Level.INFO, "Invoke Non-Constant Function "+functionName+" with args ["+data+"] on "+contractAddress);
							
							String transactionHash = getConnection().sendTransaction(functionName, contractAddress, inputParameters);
							response.addResult(od, OperationStatus.SUCCESS, String.valueOf(200), OperationStatus.SUCCESS.name(), ResponseUtil.toPayload(transactionHash));
							break;
						}
						case INVOKE_CONSTANT: {
							String returnTypes = opProperties.getProperty(RETURN_TYPES);
							List<FunctionReturnType> outputParameters = new ArrayList<FunctionReturnType>();
					        if (!StringUtil.isEmpty(returnTypes)) {
					        	buildOutputParameters(returnTypes, outputParameters);
					        }
							LOG.log(Level.INFO, "Invoke Constant Function "+functionName+" with args ["+data+"] on "+contractAddress+" return types ["+returnTypes+"]");
					        
							List<Type> results = getConnection().invoke(functionName, contractAddress, inputParameters, outputParameters);
							if (results.isEmpty()) {
								response.addEmptyResult(od, OperationStatus.SUCCESS, String.valueOf(200), OperationStatus.SUCCESS.name());
							} else {
								JSONArray jsonResults = new JSONArray();
								int i = 0;
								for (FunctionReturnType frt: outputParameters) {
									Type result = results.get(i++);
									frt.convert(result, jsonResults);
								}
								LOG.log(Level.INFO, "Invoke Function "+functionName+" results ["+jsonResults+"]");
								response.addResult(od, OperationStatus.SUCCESS, String.valueOf(200), OperationStatus.SUCCESS.name(), ResponseUtil.toPayload(jsonResults.toString()));
							}
							break;
						}
					}
				} catch (ClassNotFoundException e) {
					ResponseUtil.addExceptionFailure(response, od, e);
				} catch (NoSuchMethodException e) {
					ResponseUtil.addExceptionFailure(response, od, e);
				} catch (SecurityException e) {
					ResponseUtil.addExceptionFailure(response, od, e);
				} catch (InstantiationException e) {
					ResponseUtil.addExceptionFailure(response, od, e);
				} catch (IllegalAccessException e) {
					ResponseUtil.addExceptionFailure(response, od, e);
				} catch (IllegalArgumentException e) {
					ResponseUtil.addExceptionFailure(response, od, e);
				} catch (InvocationTargetException e) {
					ResponseUtil.addExceptionFailure(response, od, e);
				} catch (TypeMappingException e) {
					ResponseUtil.addExceptionFailure(response, od, e);
				}	
			} catch (JSONException e) {
				LOG.log(Level.SEVERE, "Fail Ethereum Function Invoke. Invalid JSON in request data", e);
				ResponseUtil.addExceptionFailure(response, od, e);
			} catch (IOException e) {
				LOG.log(Level.SEVERE, "Fail Ethereum Function Invoke", e);
				ResponseUtil.addExceptionFailure(response, od, e);
			}	

		}
	}
	
	@Override
	public EthereumConnection getConnection() {
	    return (EthereumConnection) super.getConnection();
	}
	
    private static String toString(InputStream in) throws IOException {
    	ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte[] buf = new byte[8192];
        for( int len; ( len = in.read(buf) ) != -1; ) {
        	bout.write(buf, 0, len);
        }
        return bout.toString(StandardCharsets.UTF_8.toString());
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void buildOutputParameters(String returnTypes, List<FunctionReturnType> outputParameters) {
    	String[] returnTypeList = returnTypes.split(",");
		
		for (String returnType: returnTypeList) {
			Matcher matcher = pattern.matcher(returnType); 
			if (matcher.matches()) {
				try {
					String type = matcher.group(1);
					String size = matcher.group(2);
					Class<Type> typeClass = (Class<Type>) AbiTypes.getType(type);
					
					if (StringUtil.isEmpty(size)) {
						outputParameters.add(new FunctionReturnType(typeClass, new TypeReference<DynamicArray>() {}));
					} else {
						
						if (Integer.valueOf(size) > 32) {
							throw new ConnectorException("Static Array Type "+returnType+" cannot be bigger than 32");
						}
						
						TypeReference<?> arrayTypeReference = new TypeReference<Type>() {
				            @Override
				            public java.lang.reflect.Type getType() {
				                try {
									return Class.forName(STATIC_ARRAY_CLASS_PREFIX+size);
								} catch (ClassNotFoundException e) {
									throw new ConnectorException(e.getMessage());
								}
				            }
						};
						outputParameters.add(new FunctionReturnType(Integer.valueOf(size), typeClass, arrayTypeReference));
					}
				} catch (UnsupportedOperationException e) {
					throw new ConnectorException(e.getMessage());
				}
			} else {
				outputParameters.add(new FunctionReturnType(returnType));
			}	
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static List<Type> buildInputParameters(JSONArray args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, TypeMappingException {
		List<Type> inputParameters = new ArrayList<Type>();
		for (int i = 0; i < args.length(); i++) {
			JSONObject arg = args.getJSONObject(i);
			String type = arg.getString(TYPE);
			Object value = arg.get(VALUE);
			Matcher matcher = pattern.matcher(type); 
			if (matcher.matches()) {
				if (!value.getClass().isAssignableFrom(JSONArray.class)) {
					throw new IllegalArgumentException("Argument "+arg+" must contain a JSON Array based on the value of tyoe "+type);
				}
				String arrayType = matcher.group(1);
				List<?> values = parseArrayValues((JSONArray)value, arrayType);
				Class<Type> arrayTypeClass = (Class<Type>) AbiTypes.getType(arrayType);
				String firstArrayDimension = matcher.group(2);
				String secondArrayDimension = matcher.group(3);
				
				if (secondArrayDimension != null) {
					String temp = firstArrayDimension;
					firstArrayDimension = secondArrayDimension;
					secondArrayDimension = temp;
				}
				
				if (StringUtil.isEmpty(firstArrayDimension)) {
					DynamicArray dynamicArray = null;
					if (secondArrayDimension == null) {
						dynamicArray = new DynamicArray(Utils.typeMap(values, arrayTypeClass));
					} else {
						Class<Type> arrayInnerClass = (Class<Type>)Class.forName(STATIC_ARRAY_CLASS_PREFIX+secondArrayDimension);
						dynamicArray = new DynamicArray(Utils.typeMap((List<List<Type>>)values, arrayInnerClass, arrayTypeClass));
					}
					inputParameters.add(dynamicArray);
				} else {
					if (Integer.valueOf(firstArrayDimension) > 32) {
						throw new ConnectorException("Static Array Type "+type+" cannot be bigger than 32");
					}
					Class arrayClass = Class.forName(STATIC_ARRAY_CLASS_PREFIX+firstArrayDimension);
					Constructor c = arrayClass.getConstructor(List.class);
					StaticArray staticArray = null;
					if (secondArrayDimension == null) {
						staticArray = (StaticArray) c.newInstance(Utils.typeMap(values, arrayTypeClass));
					} else {
						Class<Type> arrayInnerClass = (Class<Type>)Class.forName(STATIC_ARRAY_CLASS_PREFIX+secondArrayDimension);
						staticArray = (StaticArray) c.newInstance(Utils.typeMap((List<List<Type>>)values, arrayInnerClass, arrayTypeClass));
					}
					inputParameters.add(staticArray);
				}
			} else {
				/*
				Class<Type> typeClass = (Class<Type>) AbiTypes.getType(type);
				LOG.log(Level.FINE, "value ["+value+"] type ["+type+"] typeClass ["+typeClass.getName()+"]");
				Type result = TypeDecoder.decode(value.toString(), 0, typeClass);
				inputParameters.add(result);
				*/
				inputParameters.add(createType(arg));
			}	
		}
		return inputParameters;
	}
	
	@SuppressWarnings("rawtypes")
	private static Type createType(JSONObject arg) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Type result = null;
		String type = arg.getString(TYPE);
		@SuppressWarnings("unchecked")
		Class<Type> typeClass = (Class<Type>) AbiTypes.getType(type);
		LOG.log(Level.INFO, "value ["+arg.get(VALUE)+"] type ["+type+"] typeClass ["+typeClass.getName()+"]");
        if (NumericType.class.isAssignableFrom(typeClass)) {
        	try {
        		long l = arg.getLong(VALUE);
        		BigInteger value = BigInteger.valueOf(l);
        		result = typeClass.getConstructor(BigInteger.class).newInstance(value);
        		LOG.log(Level.INFO, "numeric value ["+value+"] result["+result+"]");
        	} catch (JSONException e) {
        		result = TypeDecoder.decode(arg.get(VALUE).toString(), 0, typeClass);
        		LOG.log(Level.SEVERE, "value conversion failed ["+arg.getBoolean(VALUE)+"] result["+result+"] exception ["+e.getLocalizedMessage()+"] attempted to convert from string");
        	}
        } else if (Bool.class.isAssignableFrom(typeClass)) {
        	result = new Bool(arg.getBoolean(VALUE));
    		LOG.log(Level.INFO, "boolean value ["+arg.getBoolean(VALUE)+"] result["+result+"]");
        } else if (Utf8String.class.isAssignableFrom(typeClass)) {
        	result = new Utf8String(arg.getString(VALUE));
    		LOG.log(Level.INFO, "utf8 string value ["+arg.getString(VALUE)+"] result["+result+"]");
        } else if (DynamicBytes.class.isAssignableFrom(typeClass)) {
        	byte[] bytes = Numeric.hexStringToByteArray(arg.getString(VALUE));
        	result = new DynamicBytes(bytes);
    		LOG.log(Level.INFO, "bytes  value ["+arg.getString(VALUE)+"] result["+result+"]");
        } else {
        	result = TypeDecoder.decode(Numeric.cleanHexPrefix(arg.getString(VALUE)), 0, typeClass);
    		LOG.log(Level.INFO, "string value ["+Numeric.cleanHexPrefix(arg.getString(VALUE))+"] result["+result+"]");
        }
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private static List<?> parseArrayValues(JSONArray value, String type) {
		List<?> valueList = null;

		for (int i = 0; i < value.length(); i++) {
			Object v = value.get(i);
			if (v.getClass().isAssignableFrom(JSONArray.class)) {
				JSONArray inner = (JSONArray)v;
				if (valueList == null) {
					valueList = new ArrayList<List<?>>();
				}
				List<?> innerList = null;
				for (int j = 0; j < inner.length(); j++) {
					if (type.startsWith("uint") || type.startsWith("int")) {
						if (innerList == null) {
							innerList = new ArrayList<BigInteger>();
							((ArrayList<List<?>>)valueList).add(innerList);
						}
						((List<BigInteger>) innerList).add(BigInteger.valueOf(inner.getInt(j)));
					} else if (type.equals("bool")) {
						if (innerList == null) {
							innerList = new ArrayList<Boolean>();
							((ArrayList<List<?>>)valueList).add(innerList);
						}
						((List<Boolean>) innerList).add(inner.getBoolean(j));
						
					} else {
						if (innerList == null) {
							innerList = new ArrayList<String>();
							((ArrayList<List<?>>)valueList).add(innerList);
						}
						((List<String>) innerList).add(inner.get(j).toString());
					}
				}
			} else {
				if (type.startsWith("uint") || type.startsWith("int")) {
					if (valueList == null) {
						valueList = new ArrayList<BigInteger>();
					}
					((List<BigInteger>) valueList).add(BigInteger.valueOf(value.getInt(i)));
				} else if (type.equals("bool")) {
					if (valueList == null) {
						valueList = new ArrayList<Boolean>();
					}
					((List<Boolean>) valueList).add(value.getBoolean(i));
				} else {
					if (valueList == null) {
						valueList = new ArrayList<String>();
					}
					((List<String>) valueList).add(value.get(i).toString());
				}
			}	
		}
		return valueList;
	}
	
}
