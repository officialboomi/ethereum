package com.boomi.connector.ethereum;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.AbiTypes;
//import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
//import org.web3j.crypto.Wallet;
//import org.web3j.crypto.WalletFile;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthCall;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
//import org.web3j.tx.RawTransactionManager;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.DefaultGasProvider;
import org.web3j.tx.gas.StaticGasProvider;
import org.web3j.utils.Numeric;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.ethereum.decode.ArrayTypeDecoder;
import com.boomi.connector.ethereum.decode.FunctionReturnType;
import com.boomi.connector.ethereum.decode.TypeDecoder;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.StringUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class EthereumConnection extends BaseConnection {
    static final int MAX_BYTE_LENGTH_FOR_HEX_STRING = Type.MAX_BYTE_LENGTH << 1;
	
    private static final Logger LOG = Logger.getLogger("com.boomi.connector.ethereum");
	//private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String PRIVATE_KEY = "privateKey";
    private static final String HOST = "host";
    private static final String PORT = "port";
    private static final String URL_SUFFIX = "urlSuffix";
    private static final String IS_SECURE = "isSecure";
    private static final String HTTP_URL_PREFIX = "http://";
    private static final String HTTPS_URL_PREFIX = "https://";
    private static final char COLON = ':';

	private final Web3j web3j;
	private final ContractGasProvider gasProvider;
	private Credentials credentials;
	private TransactionManager transactionManager;
	
	public EthereumConnection(BrowseContext context) {
		super(context);
		
		final String privateKey = context.getConnectionProperties().getProperty(PRIVATE_KEY);
        if (StringUtil.isEmpty(privateKey)) {
            throw new IllegalStateException("An Ethereum Private Key must be provided");
        }
		
		String host = context.getConnectionProperties().getProperty(HOST);
        if (StringUtil.isEmpty(host)) {
            throw new IllegalStateException("An RPC host must be provided");
        }
        
		Long port = context.getConnectionProperties().getLongProperty(PORT);
		if (port == null || port < 1 ) {
			throw new IllegalStateException("An RPC port must be provided");
		}

		String urlSuffix = context.getConnectionProperties().getProperty(URL_SUFFIX);
        if (StringUtil.isEmpty(urlSuffix)) {
        	urlSuffix = "";
        }
        
        Boolean isSecure = context.getConnectionProperties().getBooleanProperty(IS_SECURE, false);
		final String username = context.getConnectionProperties().getProperty(USERNAME);
		final String password = (String) context.getConnectionProperties().get(PASSWORD);
        
		String url = isSecure ? HTTPS_URL_PREFIX+host+COLON+port+urlSuffix : HTTP_URL_PREFIX+host+COLON+port+urlSuffix; 
		LOG.log(Level.INFO, "Ethereum Connection URL "+url);

		if (!StringUtil.isEmpty(username) && !StringUtil.isEmpty(password)) {
			OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
			clientBuilder.authenticator(new Authenticator() {
			    @Override public Request authenticate(Route route, Response response) throws IOException {
			        String credential = okhttp3.Credentials.basic(username, password);
			        return response.request().newBuilder().header("Authorization", credential).build();
			    }
			});
			web3j = Web3j.build(new HttpService(url, clientBuilder.build(), false));
		} else {
			web3j = Web3j.build(new HttpService(url));
		}
		
        gasProvider = new StaticGasProvider(DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);

		try {
			Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().send();
			LOG.log(Level.INFO, "Ethereum Client Version "+web3ClientVersion.getWeb3ClientVersion());
			/*
			WalletFile walletFile = objectMapper.readValue(keystore.getBytes(), WalletFile.class);
			credentials = Credentials.create(Wallet.decrypt(password, walletFile));
			*/
			credentials = Credentials.create(privateKey);
			//transactionManager = new RawTransactionManager(web3j, credentials);
			transactionManager = new EthereumTransactionManager(web3j, credentials);
		} catch (JsonParseException e) {
			LOG.log(Level.SEVERE, "Fail to connect to Ethereum ["+url+"]", e);
			throw new ConnectorException(e);
		} catch (JsonMappingException e) {
			LOG.log(Level.SEVERE, "Fail to connect to Ethereum ["+url+"]", e);
			throw new ConnectorException(e);
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Fail to connect to Ethereum ["+url+"]", e);
			throw new ConnectorException(e);
		/*	
		} catch (CipherException e) {
			LOG.log(Level.SEVERE, "Fail to connect to Ethereum ["+url+"]", e);
			throw new ConnectorException(e);
		*/	
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Type> invoke(String functionName, String contractAddress, List<Type> inputParameters, List<FunctionReturnType> outputParameters) throws IOException {
        final Function function = new Function(functionName, inputParameters, new ArrayList<TypeReference<?>>());
		LOG.log(Level.INFO, "Encoded Function "+functionName+"  ["+FunctionEncoder.encode(function)+"]");
        
        String encodedFunction = FunctionEncoder.encode(function);
        
		EthCall ethCall = web3j.ethCall(
				Transaction.createEthCallTransaction(transactionManager.getFromAddress(), 
						                             contractAddress, 
						                             encodedFunction),
		        DefaultBlockParameterName.LATEST)
		        .send();
        String value = ethCall.getValue();
        if (StringUtil.isEmpty(value)) {
        	throw new IOException("Invoke return value is empty for "+functionName+ " in contract "+contractAddress);
        }
        String input = Numeric.cleanHexPrefix(value);
        
		LOG.log(Level.INFO, "Returned value for  "+functionName+"  ["+input+"]");
        List<Type> results = new ArrayList<>(outputParameters.size());

        int offset = 0;
        
        for (FunctionReturnType returnType : outputParameters) {
            int hexStringDataOffset = ArrayTypeDecoder.getDataOffset(input, offset, returnType);

            Type result = null;
            if (returnType.getType().equals(FunctionReturnType.DYNAMIC_ARRAY)) {
                result = ArrayTypeDecoder.decodeDynamicArray(input, hexStringDataOffset, (TypeReference<Type>)returnType.getArrayTypeReference(), returnType.getArrayParamiterizedClass());
                offset += MAX_BYTE_LENGTH_FOR_HEX_STRING;
            } else if (returnType.getType().equals(FunctionReturnType.STATIC_ARRAY)) {
            	int length = returnType.getArraySize();
                result = ArrayTypeDecoder.decodeStaticArray(input, hexStringDataOffset, (TypeReference<Type>)returnType.getArrayTypeReference(), returnType.getArrayParamiterizedClass(), length);
                offset += length * MAX_BYTE_LENGTH_FOR_HEX_STRING;
            } else {
            	Class<Type> type = (Class<Type>)AbiTypes.getType(returnType.getType());
            	result = TypeDecoder.decode(input, hexStringDataOffset, type);
                offset += MAX_BYTE_LENGTH_FOR_HEX_STRING;            	
            }
            results.add(result);
        }
        
        return results;
	}
	
	
	@SuppressWarnings("rawtypes")
	public String sendTransaction(String functionName, String contractAddress, List<Type> inputParameters) throws IOException {
        final Function function = new Function(functionName, inputParameters, new ArrayList<TypeReference<?>>());
		LOG.log(Level.INFO, "Encoded Function "+functionName+"  ["+FunctionEncoder.encode(function)+"]");
    	EthSendTransaction est = transactionManager.sendTransaction(gasProvider.getGasPrice(functionName),  gasProvider.getGasLimit(functionName), contractAddress, FunctionEncoder.encode(function), BigInteger.ZERO);
    	if (est.hasError()) {
    		throw new IOException(est.getError().getMessage());
    	} else {
    		return est.getTransactionHash();
    	}
	}
}
